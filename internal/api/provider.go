package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"waitingapp/internal/service/entity"
)

// @Summary Post Provider
// @Description Post a new provider (register)
// @ID post-provider
// @Accept  json
// @Produce  json
// @Param id body entity.Provider true "Provider Information"
// @Success 204
// @Failure 400
// @Failure 500
// @Router /provider [post]
func (a *API) postProvider(ctx *gin.Context) {
	provider := new(entity.Provider)
	if err := ctx.BindJSON(provider); err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
	}
	if err := a.service.AddProvider(provider); err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
	}
	ctx.AbortWithStatus(http.StatusNoContent)
}
