package api

import (
	"context"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"log"
	"net/http"
	"time"
	_ "waitingapp/internal/api/docs"
	"waitingapp/internal/service"
)

// @title Waitingapp
// @version 1.0
// @description This is the waitingapp api.

// @contact.name Rami Akkal
// @contact.email r.akkal1@yahoo.de

// @contact.name Lars Hick
// @contact.email lars.hick@eljoth.de

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// @BasePath /
// @query.collection.format multi

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

type API struct {
	server  *http.Server
	service *service.Service
}

func New(s *service.Service) *API {
	router := gin.New()
	router.Use(gin.Recovery())

	server := &http.Server{
		Addr:    "localhost:8080",
		Handler: router,
	}

	a := &API{
		server:  server,
		service: s,
	}

	provider := router.Group("/provider")
	provider.POST("", a.postProvider)

	docs := router.Group("/docs")
	docs.GET("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return a
}

func (a *API) Run() {
	if err := a.server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func (a *API) Close() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	if err := a.server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}
