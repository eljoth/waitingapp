package service

import (
	"fmt"
	"log"
	"waitingapp/internal/service/entity"
)

type persistence interface {

}

type Service struct {
}

func New(p persistence) *Service {
	return nil
}

func (s *Service) AddProvider(provider *entity.Provider) error{
	log.Print(fmt.Sprintf("%v", provider))
	return nil
}
