package entity

type Provider struct {
	Name        string `json:"name"`
	Category    string `json:"category"`
	Street      string `json:"street"`
	HouseNumber string `json:"house_number"`
	PostalCode  string `json:"postal_code"`
	City        string `json:"city"`
	Country     string `json:"country"`
	Coordinates string `json:"coordinates"`
}
