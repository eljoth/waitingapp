package main

import (
	"context"
	"os/signal"
	"syscall"
	"waitingapp/internal/api"
	"waitingapp/internal/persistence"
	"waitingapp/internal/service"
)

func main() {

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	p := persistence.New()

	s := service.New(p)

	a := api.New(s)
	defer a.Close()
	go a.Run()

	<-ctx.Done()
}
